<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include 'misFunciones.php';

$valorABuscar = $_POST['valorABuscar'];

$mysqli = conectaBBDD();

$resultadoQuery = $mysqli->query("SELECT * FROM `articulos` WHERE CONCAT(`Nombre`, `ID`, `Num_Serie`, `Cantidad`) LIKE '%$valorABuscar%'");
$search_result = $resultadoQuery->num_rows;


if ($search_result > 0) 
{
    $output;
    $output ='<div class="row"><div class="col-12 text-center"><button id="volver" type="button" class="btn btn-buscar float-left">Volver</button></div></div>'
            . '<div class="col-8 table-diseno"> <table class="table">';
    for ($i = 0; $i < $search_result; $i++) 
    {
        $r = $resultadoQuery->fetch_array(); //leo una fila del resultado de la query
        $nombre = $r['Nombre'];
        $id = $r['ID'];
        $num_serie = $r['Num_Serie'];
        $cantidad = $r['Cantidad'];

        $output = $output . '<tr><td>' . $nombre . '</td><td>' . $id . '</td><td>' . $num_serie . '</td><td>' . $cantidad . '</td></tr>';
    }

    $output = $output . '</table></div>';
    echo $output;
}
?>

<script>
    $('#volver').click(function () { 
        $('#principal').load('index.php', {
           
        });
    });
</script>
    

