<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				
				<div class="profile-userpic">
					<img src="" class="img-responsive">
                                        Imagen del usuario
				</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Nombre del usuario
					</div>
                                </div>
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							DNI </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-envelope"></i>
							Email </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-user"></i>
							Nombre </a>
						</li>
						<li>
							<a href="#">
                                                        <i class="glyphicon glyphicon-lock"></i>
							Apellidos </a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
			   Aquí va la tabla del almacén
            </div>
		</div>
	</div>
</div>
<br>
<br>