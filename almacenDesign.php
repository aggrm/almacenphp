<?php
//Inicia la sesión del navegador en el servidor PHP o 
//la continúa si ya estuviera iniciada.
//Comprueba si la sesión está empezada.
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

include 'misFunciones.php';

$mysqli = conectaBBDD();

if (isset($_POST['valorABuscar']) != "")
{
    echo 'Llego';
    $resultadoQuery = $mysqli->query("SELECT * FROM `articulos` WHERE CONCAT(`Nombre`, `ID`, `Num_Serie`, `Cantidad`) LIKE '%$valorABuscar% ORDER BY ID'");
    $search_result = $resultadoQuery->num_rows;
    
    $listaProductos = array();
    
    for ($i = 0; $i < $search_result; $i++) 
    {
        $r = $resultadoQuery->fetch_array(); //leo una fila del resultado de la query
        $listaProductos[$i][0] = $r['Nombre'];
        $listaProductos[$i][1] = $r['ID'];
        $listaProductos[$i][2] = $r['Num_Serie'];
        $listaProductos[$i][3] = $r['Cantida'];
    }
    
} 
else 
{
    $resultadoQuery = $mysqli->query("SELECT * FROM `articulos`");
    $search_result = $resultadoQuery->num_rows;
    
    $listaProductos = array();
    
    for ($i = 0; $i < $search_result; $i++) 
    {
        $r = $resultadoQuery->fetch_array(); //leo una fila del resultado de la query
        $listaProductos[$i][0] = $r['Nombre'];
        $listaProductos[$i][1] = $r['ID'];
        $listaProductos[$i][2] = $r['Num_Serie'];
        $listaProductos[$i][3] = $r['Cantidad'];
    }
}

function filtraTabla($resultadoQuery) {
    $mysqli = conectaBBDD();
    $filtro_resultado = mysqli_query($mysqli, $resultadoQuery);
    return $filtro_resultado;
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <input id="valorABuscar" type="text" name="valorABuscar" class="valorabuscar float-left" placeholder="Valor a buscar">
            <button id="buscar" type="button"  class="btn btn-buscar float-left">Buscar</button>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-8 table-diseno">
            <form action="almacenDesign.php" method="post">

                <table id="tablaProdutos" class="table">
                    <tr>
                        <th>Nombre</th>
                        <th>ID</th>
                        <th>Num_Serie</th>
                        <th>Cantidad</th>
                    </tr>
<?php foreach ($listaProductos as $valor){ ?>
                        <tr>
                            <td id="Nombre"><?php echo $valor[0]?></td>
                            <td id="ID"><?php echo $valor[1]?></td>
                            <td id="Num_Serie"><?php echo $valor[2]?></td>
                            <td id="Cantidad"><?php echo $valor[3]?></td>
                        </tr>
<?php } ?>
                </table>
            </form> 
        </div>
    </div>
</div>


<script>
    $('#buscar').click(function () { 
        var _valorABuscar = $('#valorABuscar').val();
        $('#principal').load('filtraAlmacen.php', {
           valorABuscar : _valorABuscar,
        });
    });
    
</script>