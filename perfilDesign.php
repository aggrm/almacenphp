<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
                            <div class="profile-userpic">
                                <img class="img-responsive" src="img/lisensiado.png" alt="Cisneros 8 bit">
				</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Lisensiado Cisneros
					</div>
                                </div>
				<div class="profile-usermenu usuario-perfil">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							DNI: 31415926 </a>
                                                </li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-envelope"></i>
							Email: fraxito@amanterechoncho.com </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-user"></i>
							ID: Fraxito  </a>
						</li>
						<li>
							<a href="#">
                                                        <i class="glyphicon glyphicon-lock"></i>
							Cargo: Master of Pokédex </a>
						</li>
					</ul>
				</div>
			</div>
		</div>
        <br>
        <br>
		<div class="col-md-9">
            <div class="profile-content">
		<div class="col-12">
                    <h2>Añadir articulo</h2>
                    <form>
                        <label>Artículo</label>
                        <input id="Nombre" type="text" name="Articulo" class="span3">
                        <label>Numero de serie</label>
                        <input id="Id" type="text" name="ID" class="span3">
                        <label>ID</label>
                        <input id="Serie" type="text" name="Numserie" class="span3">
                        <label>Cantidad</label>
                        <input id="Cantidad" type="number" value="0" min="0" max="999" step="1"/>
                        <button id="boton-actualizar" class="btn btn-blue">Guardar</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
                <div class="col-12">
                    <h2>Actualizar cantidad</h2>
                    <form>
                        <label>ID</label>
                        <input id="Id2"type="text" name="Articulo" class="span3">
                        <label>Cantidad</label>
                        <input id="Cantidad2" type="number" value="0" min="0" max="999" step="1"/>
                        <button id="boton-cantidad" class="btn btn-blue">Actualizar</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
		</div>
	</div>
</div>
<script>

    $('#boton-actualizar').click(function () {
        //Leo el contenido de todas las cajas para luego poder jugar con su contenido 
        var _nombre = $('#Nombre').val();
        var _id = $('#Id').val();
        var _serie = $('#Serie').val();
        var _cantidad = $('#Cantidad').val();
        
         $('#principal').load("perfil.php", {
                cajaArticulo: _nombre,
                cajaId: _id,
                cajaSerie: _serie,
                cajaCantidad: _cantidad
            });
    });
    
    $('#boton-cantidad').click(function () {
        //Leo el contenido de todas las cajas para luego poder jugar con su contenido 
        var _id = $('#Id2').val();
        var _cantidad = $('#Cantidad2').val();
        
         $('#principal').load("Perfil2.php", {
                cajaId2: _id,
                cajaCantidad2: _cantidad
            });
    });
</script>
